import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { ControlPanelModule } from './control-panel/control-panel.module';
import {StoreService} from './shared/store.service';
import {RobotService} from './shared/robot.service';
import {SocketService} from './shared/socket.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ControlPanelModule
  ],
  providers: [StoreService, RobotService, SocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }
