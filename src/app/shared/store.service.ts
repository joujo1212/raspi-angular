import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Robot} from '../control-panel/shared/model/robot';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  public robots: BehaviorSubject<Robot[]> = new BehaviorSubject<Robot[]>(
    [
      {name: 'Robot 1', id: 'robot1'},
      {name: 'Robot 2', id: 'robot2 '}
    ]
  );
  public activeRobot: BehaviorSubject<Robot> = new BehaviorSubject<Robot>(null);

  constructor() { }
}
