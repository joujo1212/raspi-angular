import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import {SafeHtml} from './safe-html-pipe';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [],
  providers: [],
})
export class SharedModule { }
