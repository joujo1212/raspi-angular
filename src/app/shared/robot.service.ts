import { Injectable } from '@angular/core';
import {MoveData} from '../control-panel/shared/model/mode-data';
import {Action} from '../control-panel/shared/model/action.enum';
import {SocketService} from './socket.service';
import {StoreService} from './store.service';
import {Robot} from '../control-panel/shared/model/robot';
import {SolarPanelPosition} from '../control-panel/shared/model/solar-panel-position.enum';

@Injectable({
  providedIn: 'root'
})
export class RobotService {

  constructor(private socketService: SocketService, private storeService: StoreService) { }

  public ping() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.PING});
  }

  public move(data: MoveData): void {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.MOVE, data});
  }

  public getData() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.GET_BATTERY});
  }

  public setSolarPanelsPosition(position: SolarPanelPosition) {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.SET_SOLAR_PANELS_POSITION, data: {position}});
  }

  public sendCommand(command: string) {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.SEND_COMMAND, data: {command}});
  }

  public takePhoto(resolution: string = '-w 640 -h 480', quality = 50, awb = 'auto') {
    // const command = `raspistill -o /home/pi/robot/image.jpg  --nopreview --timeout 1 ${resolution} -q ${quality}`;
    const command = `raspistill -o /home/pi/robot/image.jpg  -vf -hf --nopreview --timeout 500 --awb ${awb} ${resolution} -q ${quality}`;
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.TAKE_PHOTO, data: {command}});
  }

  public turnCameraOn() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.TURN_CAMERA_ON});
  }

  public turnCameraOff() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.TURN_CAMERA_OFF});
  }

  public getBasicInfo() {
    const robot: Robot = this.storeService.activeRobot.getValue();
    this.socketService.send('robot/' + robot.id, {action: Action.GET_BASIC_INFO});
  }
}
