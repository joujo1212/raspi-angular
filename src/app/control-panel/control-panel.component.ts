import {Component, OnInit} from '@angular/core';

import { Action } from './shared/model/action.enum';
import { Event } from './shared/model/event.enum';
import { Message } from './shared/model/message';
import { SocketService } from '../shared/socket.service';
import {MatSelectChange} from '@angular/material';
import {RobotService} from '../shared/robot.service';
import {StoreService} from '../shared/store.service';
import {Robot} from './shared/model/robot';

@Component({
  selector: 'app-chat',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  messages: Message[] = [];
  ioConnection: any;
  public activeRobot: Robot;
  public robots: Robot[];
  isPinging = false;
  robotActive = false;
  robotPingTimeout;
  selectedPanels = [3, 5, 6];
  panels = [
    { id: 1, name: 'System control'},
    { id: 2, name: 'Terminal'},
    { id: 3, name: 'Move controller'},
    { id: 4, name: 'Sensor data'},
    { id: 5, name: 'Photo Vision'},
    { id: 6, name: 'Video Vision'},
  ];

  constructor(private socketService: SocketService, private storeService: StoreService, private robotService: RobotService) { }

  ngOnInit(): void {
    this.initIoConnection();
    this.storeService.robots.subscribe(robots => {
      this.robots = robots;
    });
    this.storeService.activeRobot.subscribe((activeRobot: Robot) => {
      this.activeRobot = activeRobot;
    });
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onMessage()
      .subscribe((message: Message) => {
        this.messages.push(message);
      });

    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected to server');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected from server');
      });

  }

  private connectToRobot() {
    this.socketService.onRobotChannel(this.storeService.activeRobot.getValue().id)
      .subscribe((m: Message) => {
        switch (m.action) {
          case Action.PONG:
            // TODO move all data to store
            this.robotActive = true;
            clearTimeout(this.robotPingTimeout);
            // If robot is responding, get basic info
            this.robotService.getBasicInfo();
            this.isPinging = false;
            break;
          case Action.BASIC_INFO:
            this.storeService.activeRobot.next({...this.activeRobot, basicInfo: m.data});
            break;
          default:
            break;
        }
      });
  }

  public handleChangeRobot(event: MatSelectChange) {
    this.robotActive = false;
    this.isPinging = false;
    this.activeRobot = null;
    this.storeService.activeRobot.next(event.value);
    this.connectToRobot();
    this.pingRobot();
  }

  public pingRobot() {
    this.robotPingTimeout = setTimeout(() => {
      this.robotActive = false;
      this.isPinging = false;
    }, 5000);
    this.isPinging = true;
    this.robotService.ping();
  }

  public preventDefault(event) {
    event.preventDefault();
    return false;
  }
}
