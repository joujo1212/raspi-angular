export interface MoveData {
  left: number; // 0 - 1
  right: number; // 0 - 1
  dir: number; // 0 - stop, -1 - backward, 1 - forward
}
