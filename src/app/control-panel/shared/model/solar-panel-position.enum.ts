export enum SolarPanelPosition {
  OPEN = 'OPEN',
  CLOSE = 'CLOSE',
  AUTO = 'AUTO'
}
