import {Component, Input, OnInit} from '@angular/core';
import {Action} from '../../model/action.enum';
import {SocketService} from '../../../../shared/socket.service';
import {RobotService} from '../../../../shared/robot.service';
import {Message} from '../../model/message';
import {StoreService} from '../../../../shared/store.service';
import {Robot} from '../../model/robot';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-photo-vision',
  templateUrl: './photo-vision.component.html',
  styleUrls: ['./photo-vision.component.css']
})
export class PhotoVisionComponent implements OnInit {
  public batteryLevel = 0;
  public showImage = true;
  public showSettings = false;
  private activeRobot: Robot;
  private robotSub: Subscription;
  public image;
  public imageName;
  public resolution = ' -w 640 -h 480';
  public resolutions = [
    {value: ' -w 320 -h 240', label: '320x340'},
    {value: ' -w 640 -h 480', label: '640x480'},
    {value: ' -w 1024 -h 768', label: '1024x768'},
    {value: ' -w 1920 -h 1080', label: '1920x1080'},
  ];
  public quality = 50;
  public qualities = [ 5, 10, 15, 20, 30, 40, 50, 70, 80, 90, 100];
  public awbs = ['off', 'auto', 'sun', 'cloudshade', 'tungsten', 'fluorescent', 'incandescent', 'flash', 'horizon'];
  public awb = 'auto';
  public takingPhoto = false;

  constructor(private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {
    this.storeService.activeRobot.subscribe(activeRobot => {
      this.activeRobot = activeRobot;
      if (this.robotSub) {
        this.robotSub.unsubscribe();
      }
      this.robotSub = this.socketService.onRobotChannel(this.activeRobot.id)
        .subscribe((m: Message) => {
          switch (m.action) {
            case Action.TAKE_PHOTO_RESPONSE:
              this.image = m.data.imageBase64;
              this.imageName = m.data.imageName;
              this.takingPhoto = false;
              break;
          }
        });
    });
  }

  public takePhoto() {
    this.robotService.takePhoto(this.resolution, this.quality, this.awb);
    this.takingPhoto = true;
  }

  public toggleImage() {
    this.showImage = !this.showImage;
  }

  public toggleSettings() {
    this.showSettings = !this.showSettings;
  }

  public downloadPhoto() {
    const link = document.createElement('a');
    link.download = this.imageName;
    link.href = 'data:image/jpeg;base64, ' + this.image;
    link.click();
  }
}
