import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewRef} from '@angular/core';
import {Action} from '../../model/action.enum';
import {SocketService} from '../../../../shared/socket.service';
import {RobotService} from '../../../../shared/robot.service';
import {Message} from '../../model/message';
import {StoreService} from '../../../../shared/store.service';
import {Robot} from '../../model/robot';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-video-vision',
  templateUrl: './video-vision.component.html',
  styleUrls: ['./video-vision.component.css']
})
export class VideoVisionComponent implements OnInit, OnDestroy {
  public activeRobot: Robot;
  private robotSub: Subscription;
  public IP;
  public showSettings = false;
  public isCameraOn = false;
  public isVrOn = false;
  public waitingForVideo = false;
  @ViewChild('target') target: ElementRef;

  constructor(private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {
    this.storeService.activeRobot.subscribe(activeRobot => {
      this.activeRobot = activeRobot;
      if (this.activeRobot.basicInfo) {
        this.IP = this.activeRobot.basicInfo.localIp;
      }
      if (this.robotSub) {
        this.robotSub.unsubscribe();
      }
      this.robotSub = this.socketService.onRobotChannel(this.activeRobot.id)
        .subscribe((m: Message) => {
          switch (m.action) {
          }
        });
    });
  }

  ngOnDestroy() {
    this.robotService.turnCameraOff();
  }

  public toggleSettings() {
    this.showSettings = !this.showSettings;
  }

  public toggleVR() {
    this.isVrOn = !this.isVrOn;
  }

  public toggleCamera() {
    if (this.isCameraOn) {
      this.robotService.turnCameraOff();
      this.waitingForVideo = false;
    } else {
      this.robotService.turnCameraOn();
      this.waitingForVideo = true;
      this.target.nativeElement.scrollIntoView();
    }
    this.isCameraOn = !this.isCameraOn;
  }

  public tryToLoadImage() {
    console.log('tryToLoadImage');
    this.isCameraOn = false;
    setTimeout(() => {
      this.isCameraOn = true;
      console.log('uz', this.IP);
      this.waitingForVideo = false;
    }, 1000);
  }

  public downloadPhoto() {
    const link = document.createElement('a');
    link.download = `video-snapshot-${Date.now()}.jpg`;
    link.href = 'http://' + this.IP + ':8080/?action=snapshot';
    link.target = '_blank';

    link.click();
  }
}
