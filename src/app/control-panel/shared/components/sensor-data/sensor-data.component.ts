import {Component, Input, OnInit} from '@angular/core';
import {Action} from '../../model/action.enum';
import {SocketService} from '../../../../shared/socket.service';
import {RobotService} from '../../../../shared/robot.service';
import {Message} from '../../model/message';
import {StoreService} from '../../../../shared/store.service';

@Component({
  selector: 'app-sensor-data',
  templateUrl: './sensor-data.component.html',
  styleUrls: ['./sensor-data.component.css']
})
export class SensorDataComponent implements OnInit {
  private activeRobot;
  public batteryLevel = 0;

  constructor(private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {
    this.socketService.onRobotChannel(this.activeRobot)
      .subscribe((m: Message) => {
        switch (m.action) {
          case Action.BATTERY:
            this.batteryLevel = m.data.batteryLevel;
            break;
        }
      });

    this.storeService.activeRobot.subscribe(activeRobot => {
      this.activeRobot = activeRobot;
    });
  }

  public getData() {
    this.robotService.getData();
  }

  public autoRefreshChanged(event) {
    console.log(event);
  }

  public getBattery() {
    this.socketService.send('robot/' + this.activeRobot, {action: Action.GET_BATTERY});
  }
}
