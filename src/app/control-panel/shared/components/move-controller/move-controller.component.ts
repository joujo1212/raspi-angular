import {AfterViewInit, Component, OnInit} from '@angular/core';
import Manager from 'nipplejs';
import {RobotService} from '../../../../shared/robot.service';

@Component({
  selector: 'app-move-controller',
  templateUrl: './move-controller.component.html',
  styleUrls: ['./move-controller.component.css']
})
export class MoveControllerComponent implements OnInit, AfterViewInit {
  private threshold = 0.05;
  private controller_right = null;
  private radius = 150;
  private options = {
    zone: null,                  // active zone
    color: '#00bcd4',
    size: this.radius,
    threshold: 0.1,               // before triggering a directional event
    fadeTime: 500,              // transition time
    multitouch: false,
    maxNumberOfNipples: 1,     // when multitouch, what is too many?
    dataOnly: false,              // no dom element whatsoever
    // position: {top: this.position + 'px', left: this.position + 'px'},               // preset position for 'static' mode
    mode: 'dynamic',                   // 'dynamic', 'static' or 'semi'
    restOpacity: 0.5,            // opacity when not 'dynamic' and rested
    catchDistance: 200           // distance to recycle previous joystick in 'semi' mode
  };
  lastX = 0;
  lastY = 0;
  lastLeft = 0;
  lastRight = 0;
  lastDir = 0;

  constructor(private robotService: RobotService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // Component views are initialized
    this.options.zone = document.getElementById('pan-controller-right');
    this.controller_right = Manager.create(this.options);
    this.controller_right.on('move', this.controllerRightOnMove.bind(this));
    this.controller_right.on('end', this.controllerRightOnEnd.bind(this));
  }

  private controllerRightOnMove(event, data) {
    // console.log(data);
    let x = Math.floor((data.position.x - data.instance.position.x)) / this.radius * 2;
    x = Math.floor(x * 100) / 100; // -1 - +1
    let y = Math.floor((data.position.y - data.instance.position.y)) / this.radius * 2;
    y = Math.floor(y * 100) / 100; // -1 - +1
    // console.log(x, y);

    if (Math.abs(x - this.lastX) >= this.threshold || Math.abs(y - this.lastY) >= this.threshold) {
      let left;
      let right;
      let dir; // 1 forward, -1 backward

      left = (1 + x) * (-y);
      if (left < 0) {
        left = Math.abs(left);
      }
      if (left > 1) {
        left = 1;
      }
      right = (x - 1) * y;
      if (right < 0) {
        right = Math.abs(right);
      }
      if (right > 1) {
        right = 1;
      }
      left = Math.floor(left * 100) / 100; // 0 - 1
      right = Math.floor(right * 100) / 100; // 0 - 1

      dir = y < 0 ? 1 : -1;

      this.robotService.move({left, right, dir});
      this.lastX = x;
      this.lastY = y;
      this.lastLeft = left;
      this.lastRight = right;
      this.lastDir = dir;
    }
  }

  private controllerRightOnEnd() {
    this.robotService.move({left: 0, right: 0, dir: 0});
    this.lastLeft = 0;
    this.lastRight = 0;
    this.lastDir = 0;
  }
}
