import { Component, OnInit } from '@angular/core';
import {RobotService} from '../../../../shared/robot.service';

const KEYS = {
  up: 38,
  down: 40,
  left: 37,
  right:  39
};

@Component({
  selector: 'app-keyboard-controller',
  templateUrl: './keyboard-controller.component.html',
  styleUrls: ['./keyboard-controller.component.css']
})
export class KeyboardControllerComponent implements OnInit {
  private isUp = true;
  public allowKeyboard = true;
  public speed = 0.8;
  public dir = 0;
  public left = 0;
  public right = 0;
  // data from accelerometer
  public data = '0 0 0';
  private maxX = 1;
  private maxY = 1;
  constructor(private robotService: RobotService) { }

  handleOrientation(event) {
    // this.data = `${event.alpha}, ${event.beta}, ${event.gamma}, ${event.absolute}`;
    let x = event.beta;  // In degree in the range [-180,180]
    let y = event.gamma; // In degree in the range [-90,90]

    // this.data  = "beta : " + x + "\n";
    // this.data += "gamma: " + y + "\n";

    // Because we don't want to have the device upside down
    // We constrain the x value to the range [-90,90]
    if (x >  90) { x =  90; }
    if (x < -90) { x = -90; }

    // To make computation easier we shift the range of
    // x and y to [0,180]
    x += 90;
    y += 90;

    // 10 is half the size of the ball
    // It center the positioning point to the center of the ball
    // ball.style.top  = (maxX*x/180) + "px";
    // ball.style.left = (maxY*y/180) + "px";
    this.data = `${this.maxX * x / 180} ${this.maxY * y / 180}`;
  }

  ngOnInit() {
    window.addEventListener('deviceorientation', this.handleOrientation.bind(this), true);
    window.onkeyup = (e) => {
      if (this.allowKeyboard) {
        const key = e.keyCode ? e.keyCode : e.which;
        console.log(key);
        switch (key) {
          case KEYS.up:
          case KEYS.down:
          case KEYS.left:
          case KEYS.right:
            this.isUp = true;
            this.dir = this.left = this.right = 0;
            this.move();
        }
        e.preventDefault();
      }
    };

    window.onkeydown = (e) => {
      if (this.allowKeyboard) {
        if (!this.isUp) {
          e.preventDefault();
          return;
        }

        const key = e.keyCode ? e.keyCode : e.which;
        console.log(key);
        switch (key) {
          case KEYS.up:
            e.preventDefault();
            this.isUp = false;
            this.dir = 1;
            this.left = this.right = this.speed;
            this.move();
            break;
          case KEYS.down:
            e.preventDefault();
            this.isUp = false;
            this.dir = -1;
            this.left = this.right = this.speed;
            this.move();
            break;
          case KEYS.left:
            e.preventDefault();
            this.isUp = false;
            this.dir = 1;
            this.left = 0;
            this.right = this.speed;
            this.move();
            break;
          case KEYS.right:
            e.preventDefault();
            this.isUp = false;
            this.dir = 1;
            this.left = this.speed;
            this.right = 0;
            this.move();
            break;
        }
      }
    };

  }

  private move() {
    this.robotService.move({left: this.left, right: this.right, dir: this.dir});
  }

  public controlWithKeyboardChanged({checked}) {
    this.allowKeyboard = checked;
  }

  public handleChangeSpeed({value}) {
    console.log(event);
    this.speed = value;
  }

  public renderThumb(value: number) {
    return `${Math.round(value * 100)}%`;
  }
}
