import { Component, OnInit } from '@angular/core';
import {SolarPanelPosition} from '../../model/solar-panel-position.enum';
import {RobotService} from '../../../../shared/robot.service';
import {StoreService} from '../../../../shared/store.service';
import {SocketService} from '../../../../shared/socket.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-system-controls',
  templateUrl: './system-controls.component.html',
  styleUrls: ['./system-controls.component.css']
})
export class SystemControlsComponent implements OnInit {
  solarPosition = SolarPanelPosition;

  constructor(public sanitizer: DomSanitizer, private robotService: RobotService, private socketService: SocketService, private storeService: StoreService) { }

  ngOnInit() {}

  public solarPanelsChanged({value}) {
    this.robotService.setSolarPanelsPosition(value);
  }

}
